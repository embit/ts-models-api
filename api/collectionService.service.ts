/**
 * Emby Server API
 * Explore the Emby Server API
 *
 * OpenAPI spec version: 4.4.0.11
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *//* tslint:disable:no-unused-variable member-ordering */

import { Inject, Injectable, Optional }                      from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams,
         HttpResponse, HttpEvent }                           from '@angular/common/http';
import { CustomHttpUrlEncodingCodec }                        from '../encoder';

import { Observable }                                        from 'rxjs';

import { CollectionsCollectionCreationResult } from '../model/collectionsCollectionCreationResult';

import { BASE_PATH, COLLECTION_FORMATS }                     from '../variables';
import { Configuration }                                     from '../configuration';


@Injectable()
export class CollectionServiceService {

    protected basePath = 'https://stream.lebit.tech:8920/emby';
    public defaultHeaders = new HttpHeaders();
    public configuration = new Configuration();

    constructor(protected httpClient: HttpClient, @Optional()@Inject(BASE_PATH) basePath: string, @Optional() configuration: Configuration) {
        if (basePath) {
            this.basePath = basePath;
        }
        if (configuration) {
            this.configuration = configuration;
            this.basePath = basePath || configuration.basePath || this.basePath;
        }
    }

    /**
     * @param consumes string[] mime-types
     * @return true: consumes contains 'multipart/form-data', false: otherwise
     */
    private canConsumeForm(consumes: string[]): boolean {
        const form = 'multipart/form-data';
        for (const consume of consumes) {
            if (form === consume) {
                return true;
            }
        }
        return false;
    }


    /**
     * Removes items from a collection
     * Requires authentication as user
     * @param ids Item id, comma delimited
     * @param id 
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public deleteCollectionsByIdItems(ids: string, id: string, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public deleteCollectionsByIdItems(ids: string, id: string, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public deleteCollectionsByIdItems(ids: string, id: string, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public deleteCollectionsByIdItems(ids: string, id: string, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (ids === null || ids === undefined) {
            throw new Error('Required parameter ids was null or undefined when calling deleteCollectionsByIdItems.');
        }

        if (id === null || id === undefined) {
            throw new Error('Required parameter id was null or undefined when calling deleteCollectionsByIdItems.');
        }

        let queryParameters = new HttpParams({encoder: new CustomHttpUrlEncodingCodec()});
        if (ids !== undefined && ids !== null) {
            queryParameters = queryParameters.set('Ids', <any>ids);
        }

        let headers = this.defaultHeaders;

        // authentication (apikeyauth) required
        if (this.configuration.apiKeys["api_key"]) {
            queryParameters = queryParameters.set('api_key', this.configuration.apiKeys["api_key"]);
        }

        // authentication (embyauth) required
        if (this.configuration.accessToken) {
            const accessToken = typeof this.configuration.accessToken === 'function'
                ? this.configuration.accessToken()
                : this.configuration.accessToken;
            headers = headers.set('Authorization', 'Bearer ' + accessToken);
        }
        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.request<any>('delete',`${this.basePath}/Collections/${encodeURIComponent(String(id))}/Items`,
            {
                params: queryParameters,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * Creates a new collection
     * Requires authentication as user
     * @param isLocked Whether or not to lock the new collection.
     * @param name The name of the new collection.
     * @param parentId Optional - create the collection within a specific folder
     * @param ids Item Ids to add to the collection
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public postCollections(isLocked?: boolean, name?: string, parentId?: string, ids?: string, observe?: 'body', reportProgress?: boolean): Observable<CollectionsCollectionCreationResult>;
    public postCollections(isLocked?: boolean, name?: string, parentId?: string, ids?: string, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<CollectionsCollectionCreationResult>>;
    public postCollections(isLocked?: boolean, name?: string, parentId?: string, ids?: string, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<CollectionsCollectionCreationResult>>;
    public postCollections(isLocked?: boolean, name?: string, parentId?: string, ids?: string, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {





        let queryParameters = new HttpParams({encoder: new CustomHttpUrlEncodingCodec()});
        if (isLocked !== undefined && isLocked !== null) {
            queryParameters = queryParameters.set('IsLocked', <any>isLocked);
        }
        if (name !== undefined && name !== null) {
            queryParameters = queryParameters.set('Name', <any>name);
        }
        if (parentId !== undefined && parentId !== null) {
            queryParameters = queryParameters.set('ParentId', <any>parentId);
        }
        if (ids !== undefined && ids !== null) {
            queryParameters = queryParameters.set('Ids', <any>ids);
        }

        let headers = this.defaultHeaders;

        // authentication (apikeyauth) required
        if (this.configuration.apiKeys["api_key"]) {
            queryParameters = queryParameters.set('api_key', this.configuration.apiKeys["api_key"]);
        }

        // authentication (embyauth) required
        if (this.configuration.accessToken) {
            const accessToken = typeof this.configuration.accessToken === 'function'
                ? this.configuration.accessToken()
                : this.configuration.accessToken;
            headers = headers.set('Authorization', 'Bearer ' + accessToken);
        }
        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'application/json',
            'application/xml'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.request<CollectionsCollectionCreationResult>('post',`${this.basePath}/Collections`,
            {
                params: queryParameters,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * Adds items to a collection
     * Requires authentication as user
     * @param ids Item id, comma delimited
     * @param id 
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public postCollectionsByIdItems(ids: string, id: string, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public postCollectionsByIdItems(ids: string, id: string, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public postCollectionsByIdItems(ids: string, id: string, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public postCollectionsByIdItems(ids: string, id: string, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (ids === null || ids === undefined) {
            throw new Error('Required parameter ids was null or undefined when calling postCollectionsByIdItems.');
        }

        if (id === null || id === undefined) {
            throw new Error('Required parameter id was null or undefined when calling postCollectionsByIdItems.');
        }

        let queryParameters = new HttpParams({encoder: new CustomHttpUrlEncodingCodec()});
        if (ids !== undefined && ids !== null) {
            queryParameters = queryParameters.set('Ids', <any>ids);
        }

        let headers = this.defaultHeaders;

        // authentication (apikeyauth) required
        if (this.configuration.apiKeys["api_key"]) {
            queryParameters = queryParameters.set('api_key', this.configuration.apiKeys["api_key"]);
        }

        // authentication (embyauth) required
        if (this.configuration.accessToken) {
            const accessToken = typeof this.configuration.accessToken === 'function'
                ? this.configuration.accessToken()
                : this.configuration.accessToken;
            headers = headers.set('Authorization', 'Bearer ' + accessToken);
        }
        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.request<any>('post',`${this.basePath}/Collections/${encodeURIComponent(String(id))}/Items`,
            {
                params: queryParameters,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

}
