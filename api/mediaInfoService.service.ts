/**
 * Emby Server API
 * Explore the Emby Server API
 *
 * OpenAPI spec version: 4.4.0.11
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *//* tslint:disable:no-unused-variable member-ordering */

import { Inject, Injectable, Optional }                      from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams,
         HttpResponse, HttpEvent }                           from '@angular/common/http';
import { CustomHttpUrlEncodingCodec }                        from '../encoder';

import { Observable }                                        from 'rxjs';

import { MediaInfoLiveStreamRequest } from '../model/mediaInfoLiveStreamRequest';
import { MediaInfoLiveStreamResponse } from '../model/mediaInfoLiveStreamResponse';
import { MediaInfoPlaybackInfoRequest } from '../model/mediaInfoPlaybackInfoRequest';
import { MediaInfoPlaybackInfoResponse } from '../model/mediaInfoPlaybackInfoResponse';

import { BASE_PATH, COLLECTION_FORMATS }                     from '../variables';
import { Configuration }                                     from '../configuration';


@Injectable()
export class MediaInfoServiceService {

    protected basePath = 'https://stream.lebit.tech:8920/emby';
    public defaultHeaders = new HttpHeaders();
    public configuration = new Configuration();

    constructor(protected httpClient: HttpClient, @Optional()@Inject(BASE_PATH) basePath: string, @Optional() configuration: Configuration) {
        if (basePath) {
            this.basePath = basePath;
        }
        if (configuration) {
            this.configuration = configuration;
            this.basePath = basePath || configuration.basePath || this.basePath;
        }
    }

    /**
     * @param consumes string[] mime-types
     * @return true: consumes contains 'multipart/form-data', false: otherwise
     */
    private canConsumeForm(consumes: string[]): boolean {
        const form = 'multipart/form-data';
        for (const consume of consumes) {
            if (form === consume) {
                return true;
            }
        }
        return false;
    }


    /**
     * Gets live playback media info for an item
     * Requires authentication as user
     * @param id Item Id
     * @param userId User Id
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public getItemsByIdPlaybackinfo(id: string, userId: string, observe?: 'body', reportProgress?: boolean): Observable<MediaInfoPlaybackInfoResponse>;
    public getItemsByIdPlaybackinfo(id: string, userId: string, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<MediaInfoPlaybackInfoResponse>>;
    public getItemsByIdPlaybackinfo(id: string, userId: string, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<MediaInfoPlaybackInfoResponse>>;
    public getItemsByIdPlaybackinfo(id: string, userId: string, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (id === null || id === undefined) {
            throw new Error('Required parameter id was null or undefined when calling getItemsByIdPlaybackinfo.');
        }

        if (userId === null || userId === undefined) {
            throw new Error('Required parameter userId was null or undefined when calling getItemsByIdPlaybackinfo.');
        }

        let queryParameters = new HttpParams({encoder: new CustomHttpUrlEncodingCodec()});
        if (userId !== undefined && userId !== null) {
            queryParameters = queryParameters.set('UserId', <any>userId);
        }

        let headers = this.defaultHeaders;

        // authentication (apikeyauth) required
        if (this.configuration.apiKeys["api_key"]) {
            queryParameters = queryParameters.set('api_key', this.configuration.apiKeys["api_key"]);
        }

        // authentication (embyauth) required
        if (this.configuration.accessToken) {
            const accessToken = typeof this.configuration.accessToken === 'function'
                ? this.configuration.accessToken()
                : this.configuration.accessToken;
            headers = headers.set('Authorization', 'Bearer ' + accessToken);
        }
        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'application/json',
            'application/xml'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.request<MediaInfoPlaybackInfoResponse>('get',`${this.basePath}/Items/${encodeURIComponent(String(id))}/PlaybackInfo`,
            {
                params: queryParameters,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * 
     * Requires authentication as user
     * @param size Size
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public getPlaybackBitratetest(size: number, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public getPlaybackBitratetest(size: number, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public getPlaybackBitratetest(size: number, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public getPlaybackBitratetest(size: number, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (size === null || size === undefined) {
            throw new Error('Required parameter size was null or undefined when calling getPlaybackBitratetest.');
        }

        let queryParameters = new HttpParams({encoder: new CustomHttpUrlEncodingCodec()});
        if (size !== undefined && size !== null) {
            queryParameters = queryParameters.set('Size', <any>size);
        }

        let headers = this.defaultHeaders;

        // authentication (apikeyauth) required
        if (this.configuration.apiKeys["api_key"]) {
            queryParameters = queryParameters.set('api_key', this.configuration.apiKeys["api_key"]);
        }

        // authentication (embyauth) required
        if (this.configuration.accessToken) {
            const accessToken = typeof this.configuration.accessToken === 'function'
                ? this.configuration.accessToken()
                : this.configuration.accessToken;
            headers = headers.set('Authorization', 'Bearer ' + accessToken);
        }
        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.request<any>('get',`${this.basePath}/Playback/BitrateTest`,
            {
                params: queryParameters,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * Gets live playback media info for an item
     * Requires authentication as user
     * @param body PlaybackInfoRequest: 
     * @param id 
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public postItemsByIdPlaybackinfo(body: MediaInfoPlaybackInfoRequest, id: string, observe?: 'body', reportProgress?: boolean): Observable<MediaInfoPlaybackInfoResponse>;
    public postItemsByIdPlaybackinfo(body: MediaInfoPlaybackInfoRequest, id: string, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<MediaInfoPlaybackInfoResponse>>;
    public postItemsByIdPlaybackinfo(body: MediaInfoPlaybackInfoRequest, id: string, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<MediaInfoPlaybackInfoResponse>>;
    public postItemsByIdPlaybackinfo(body: MediaInfoPlaybackInfoRequest, id: string, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (body === null || body === undefined) {
            throw new Error('Required parameter body was null or undefined when calling postItemsByIdPlaybackinfo.');
        }

        if (id === null || id === undefined) {
            throw new Error('Required parameter id was null or undefined when calling postItemsByIdPlaybackinfo.');
        }

        let headers = this.defaultHeaders;

        // authentication (apikeyauth) required
        if (this.configuration.apiKeys["api_key"]) {
            queryParameters = queryParameters.set('api_key', this.configuration.apiKeys["api_key"]);
        }

        // authentication (embyauth) required
        if (this.configuration.accessToken) {
            const accessToken = typeof this.configuration.accessToken === 'function'
                ? this.configuration.accessToken()
                : this.configuration.accessToken;
            headers = headers.set('Authorization', 'Bearer ' + accessToken);
        }
        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'application/json',
            'application/xml'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
            'application/json',
            'application/xml'
        ];
        const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(consumes);
        if (httpContentTypeSelected != undefined) {
            headers = headers.set('Content-Type', httpContentTypeSelected);
        }

        return this.httpClient.request<MediaInfoPlaybackInfoResponse>('post',`${this.basePath}/Items/${encodeURIComponent(String(id))}/PlaybackInfo`,
            {
                body: body,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * Closes a media source
     * Requires authentication as user
     * @param liveStreamId LiveStreamId
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public postLivestreamsClose(liveStreamId: string, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public postLivestreamsClose(liveStreamId: string, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public postLivestreamsClose(liveStreamId: string, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public postLivestreamsClose(liveStreamId: string, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (liveStreamId === null || liveStreamId === undefined) {
            throw new Error('Required parameter liveStreamId was null or undefined when calling postLivestreamsClose.');
        }

        let queryParameters = new HttpParams({encoder: new CustomHttpUrlEncodingCodec()});
        if (liveStreamId !== undefined && liveStreamId !== null) {
            queryParameters = queryParameters.set('LiveStreamId', <any>liveStreamId);
        }

        let headers = this.defaultHeaders;

        // authentication (apikeyauth) required
        if (this.configuration.apiKeys["api_key"]) {
            queryParameters = queryParameters.set('api_key', this.configuration.apiKeys["api_key"]);
        }

        // authentication (embyauth) required
        if (this.configuration.accessToken) {
            const accessToken = typeof this.configuration.accessToken === 'function'
                ? this.configuration.accessToken()
                : this.configuration.accessToken;
            headers = headers.set('Authorization', 'Bearer ' + accessToken);
        }
        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.request<any>('post',`${this.basePath}/LiveStreams/Close`,
            {
                params: queryParameters,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * Closes a media source
     * Requires authentication as user
     * @param liveStreamId LiveStreamId
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public postLivestreamsMediainfo(liveStreamId: string, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public postLivestreamsMediainfo(liveStreamId: string, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public postLivestreamsMediainfo(liveStreamId: string, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public postLivestreamsMediainfo(liveStreamId: string, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (liveStreamId === null || liveStreamId === undefined) {
            throw new Error('Required parameter liveStreamId was null or undefined when calling postLivestreamsMediainfo.');
        }

        let queryParameters = new HttpParams({encoder: new CustomHttpUrlEncodingCodec()});
        if (liveStreamId !== undefined && liveStreamId !== null) {
            queryParameters = queryParameters.set('LiveStreamId', <any>liveStreamId);
        }

        let headers = this.defaultHeaders;

        // authentication (apikeyauth) required
        if (this.configuration.apiKeys["api_key"]) {
            queryParameters = queryParameters.set('api_key', this.configuration.apiKeys["api_key"]);
        }

        // authentication (embyauth) required
        if (this.configuration.accessToken) {
            const accessToken = typeof this.configuration.accessToken === 'function'
                ? this.configuration.accessToken()
                : this.configuration.accessToken;
            headers = headers.set('Authorization', 'Bearer ' + accessToken);
        }
        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.request<any>('post',`${this.basePath}/LiveStreams/MediaInfo`,
            {
                params: queryParameters,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * Opens a media source
     * Requires authentication as user
     * @param body LiveStreamRequest: 
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public postLivestreamsOpen(body: MediaInfoLiveStreamRequest, observe?: 'body', reportProgress?: boolean): Observable<MediaInfoLiveStreamResponse>;
    public postLivestreamsOpen(body: MediaInfoLiveStreamRequest, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<MediaInfoLiveStreamResponse>>;
    public postLivestreamsOpen(body: MediaInfoLiveStreamRequest, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<MediaInfoLiveStreamResponse>>;
    public postLivestreamsOpen(body: MediaInfoLiveStreamRequest, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (body === null || body === undefined) {
            throw new Error('Required parameter body was null or undefined when calling postLivestreamsOpen.');
        }

        let headers = this.defaultHeaders;

        // authentication (apikeyauth) required
        if (this.configuration.apiKeys["api_key"]) {
            queryParameters = queryParameters.set('api_key', this.configuration.apiKeys["api_key"]);
        }

        // authentication (embyauth) required
        if (this.configuration.accessToken) {
            const accessToken = typeof this.configuration.accessToken === 'function'
                ? this.configuration.accessToken()
                : this.configuration.accessToken;
            headers = headers.set('Authorization', 'Bearer ' + accessToken);
        }
        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'application/json',
            'application/xml'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
            'application/json',
            'application/xml'
        ];
        const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(consumes);
        if (httpContentTypeSelected != undefined) {
            headers = headers.set('Content-Type', httpContentTypeSelected);
        }

        return this.httpClient.request<MediaInfoLiveStreamResponse>('post',`${this.basePath}/LiveStreams/Open`,
            {
                body: body,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

}
