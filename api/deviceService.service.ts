/**
 * Emby Server API
 * Explore the Emby Server API
 *
 * OpenAPI spec version: 4.4.0.11
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *//* tslint:disable:no-unused-variable member-ordering */

import { Inject, Injectable, Optional }                      from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams,
         HttpResponse, HttpEvent }                           from '@angular/common/http';
import { CustomHttpUrlEncodingCodec }                        from '../encoder';

import { Observable }                                        from 'rxjs';

import { DevicesContentUploadHistory } from '../model/devicesContentUploadHistory';
import { DevicesDeviceInfo } from '../model/devicesDeviceInfo';
import { DevicesDeviceOptions } from '../model/devicesDeviceOptions';
import { QueryResultDevicesDeviceInfo } from '../model/queryResultDevicesDeviceInfo';

import { BASE_PATH, COLLECTION_FORMATS }                     from '../variables';
import { Configuration }                                     from '../configuration';


@Injectable()
export class DeviceServiceService {

    protected basePath = 'https://stream.lebit.tech:8920/emby';
    public defaultHeaders = new HttpHeaders();
    public configuration = new Configuration();

    constructor(protected httpClient: HttpClient, @Optional()@Inject(BASE_PATH) basePath: string, @Optional() configuration: Configuration) {
        if (basePath) {
            this.basePath = basePath;
        }
        if (configuration) {
            this.configuration = configuration;
            this.basePath = basePath || configuration.basePath || this.basePath;
        }
    }

    /**
     * @param consumes string[] mime-types
     * @return true: consumes contains 'multipart/form-data', false: otherwise
     */
    private canConsumeForm(consumes: string[]): boolean {
        const form = 'multipart/form-data';
        for (const consume of consumes) {
            if (form === consume) {
                return true;
            }
        }
        return false;
    }


    /**
     * Deletes a device
     * Requires authentication as administrator
     * @param id Device Id
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public deleteDevices(id: string, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public deleteDevices(id: string, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public deleteDevices(id: string, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public deleteDevices(id: string, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (id === null || id === undefined) {
            throw new Error('Required parameter id was null or undefined when calling deleteDevices.');
        }

        let queryParameters = new HttpParams({encoder: new CustomHttpUrlEncodingCodec()});
        if (id !== undefined && id !== null) {
            queryParameters = queryParameters.set('Id', <any>id);
        }

        let headers = this.defaultHeaders;

        // authentication (apikeyauth) required
        if (this.configuration.apiKeys["api_key"]) {
            queryParameters = queryParameters.set('api_key', this.configuration.apiKeys["api_key"]);
        }

        // authentication (embyauth) required
        if (this.configuration.accessToken) {
            const accessToken = typeof this.configuration.accessToken === 'function'
                ? this.configuration.accessToken()
                : this.configuration.accessToken;
            headers = headers.set('Authorization', 'Bearer ' + accessToken);
        }
        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.request<any>('delete',`${this.basePath}/Devices`,
            {
                params: queryParameters,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * Gets all devices
     * Requires authentication as administrator
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public getDevices(observe?: 'body', reportProgress?: boolean): Observable<QueryResultDevicesDeviceInfo>;
    public getDevices(observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<QueryResultDevicesDeviceInfo>>;
    public getDevices(observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<QueryResultDevicesDeviceInfo>>;
    public getDevices(observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        let headers = this.defaultHeaders;

        // authentication (apikeyauth) required
        if (this.configuration.apiKeys["api_key"]) {
            queryParameters = queryParameters.set('api_key', this.configuration.apiKeys["api_key"]);
        }

        // authentication (embyauth) required
        if (this.configuration.accessToken) {
            const accessToken = typeof this.configuration.accessToken === 'function'
                ? this.configuration.accessToken()
                : this.configuration.accessToken;
            headers = headers.set('Authorization', 'Bearer ' + accessToken);
        }
        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'application/json',
            'application/xml'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.request<QueryResultDevicesDeviceInfo>('get',`${this.basePath}/Devices`,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * Gets camera upload history for a device
     * Requires authentication as user
     * @param deviceId Device Id
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public getDevicesCamerauploads(deviceId: string, observe?: 'body', reportProgress?: boolean): Observable<DevicesContentUploadHistory>;
    public getDevicesCamerauploads(deviceId: string, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<DevicesContentUploadHistory>>;
    public getDevicesCamerauploads(deviceId: string, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<DevicesContentUploadHistory>>;
    public getDevicesCamerauploads(deviceId: string, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (deviceId === null || deviceId === undefined) {
            throw new Error('Required parameter deviceId was null or undefined when calling getDevicesCamerauploads.');
        }

        let queryParameters = new HttpParams({encoder: new CustomHttpUrlEncodingCodec()});
        if (deviceId !== undefined && deviceId !== null) {
            queryParameters = queryParameters.set('DeviceId', <any>deviceId);
        }

        let headers = this.defaultHeaders;

        // authentication (apikeyauth) required
        if (this.configuration.apiKeys["api_key"]) {
            queryParameters = queryParameters.set('api_key', this.configuration.apiKeys["api_key"]);
        }

        // authentication (embyauth) required
        if (this.configuration.accessToken) {
            const accessToken = typeof this.configuration.accessToken === 'function'
                ? this.configuration.accessToken()
                : this.configuration.accessToken;
            headers = headers.set('Authorization', 'Bearer ' + accessToken);
        }
        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'application/json',
            'application/xml'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.request<DevicesContentUploadHistory>('get',`${this.basePath}/Devices/CameraUploads`,
            {
                params: queryParameters,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * Gets info for a device
     * Requires authentication as administrator
     * @param id Device Id
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public getDevicesInfo(id: string, observe?: 'body', reportProgress?: boolean): Observable<DevicesDeviceInfo>;
    public getDevicesInfo(id: string, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<DevicesDeviceInfo>>;
    public getDevicesInfo(id: string, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<DevicesDeviceInfo>>;
    public getDevicesInfo(id: string, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (id === null || id === undefined) {
            throw new Error('Required parameter id was null or undefined when calling getDevicesInfo.');
        }

        let queryParameters = new HttpParams({encoder: new CustomHttpUrlEncodingCodec()});
        if (id !== undefined && id !== null) {
            queryParameters = queryParameters.set('Id', <any>id);
        }

        let headers = this.defaultHeaders;

        // authentication (apikeyauth) required
        if (this.configuration.apiKeys["api_key"]) {
            queryParameters = queryParameters.set('api_key', this.configuration.apiKeys["api_key"]);
        }

        // authentication (embyauth) required
        if (this.configuration.accessToken) {
            const accessToken = typeof this.configuration.accessToken === 'function'
                ? this.configuration.accessToken()
                : this.configuration.accessToken;
            headers = headers.set('Authorization', 'Bearer ' + accessToken);
        }
        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'application/json',
            'application/xml'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.request<DevicesDeviceInfo>('get',`${this.basePath}/Devices/Info`,
            {
                params: queryParameters,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * Gets options for a device
     * Requires authentication as administrator
     * @param id Device Id
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public getDevicesOptions(id: string, observe?: 'body', reportProgress?: boolean): Observable<DevicesDeviceOptions>;
    public getDevicesOptions(id: string, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<DevicesDeviceOptions>>;
    public getDevicesOptions(id: string, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<DevicesDeviceOptions>>;
    public getDevicesOptions(id: string, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (id === null || id === undefined) {
            throw new Error('Required parameter id was null or undefined when calling getDevicesOptions.');
        }

        let queryParameters = new HttpParams({encoder: new CustomHttpUrlEncodingCodec()});
        if (id !== undefined && id !== null) {
            queryParameters = queryParameters.set('Id', <any>id);
        }

        let headers = this.defaultHeaders;

        // authentication (apikeyauth) required
        if (this.configuration.apiKeys["api_key"]) {
            queryParameters = queryParameters.set('api_key', this.configuration.apiKeys["api_key"]);
        }

        // authentication (embyauth) required
        if (this.configuration.accessToken) {
            const accessToken = typeof this.configuration.accessToken === 'function'
                ? this.configuration.accessToken()
                : this.configuration.accessToken;
            headers = headers.set('Authorization', 'Bearer ' + accessToken);
        }
        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'application/json',
            'application/xml'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.request<DevicesDeviceOptions>('get',`${this.basePath}/Devices/Options`,
            {
                params: queryParameters,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * Uploads content
     * Requires authentication as user
     * @param body Binary stream
     * @param deviceId Device Id
     * @param album Album
     * @param name Name
     * @param id Id
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public postDevicesCamerauploads(body: Object, deviceId: string, album: string, name: string, id: string, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public postDevicesCamerauploads(body: Object, deviceId: string, album: string, name: string, id: string, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public postDevicesCamerauploads(body: Object, deviceId: string, album: string, name: string, id: string, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public postDevicesCamerauploads(body: Object, deviceId: string, album: string, name: string, id: string, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (body === null || body === undefined) {
            throw new Error('Required parameter body was null or undefined when calling postDevicesCamerauploads.');
        }

        if (deviceId === null || deviceId === undefined) {
            throw new Error('Required parameter deviceId was null or undefined when calling postDevicesCamerauploads.');
        }

        if (album === null || album === undefined) {
            throw new Error('Required parameter album was null or undefined when calling postDevicesCamerauploads.');
        }

        if (name === null || name === undefined) {
            throw new Error('Required parameter name was null or undefined when calling postDevicesCamerauploads.');
        }

        if (id === null || id === undefined) {
            throw new Error('Required parameter id was null or undefined when calling postDevicesCamerauploads.');
        }

        let queryParameters = new HttpParams({encoder: new CustomHttpUrlEncodingCodec()});
        if (deviceId !== undefined && deviceId !== null) {
            queryParameters = queryParameters.set('DeviceId', <any>deviceId);
        }
        if (album !== undefined && album !== null) {
            queryParameters = queryParameters.set('Album', <any>album);
        }
        if (name !== undefined && name !== null) {
            queryParameters = queryParameters.set('Name', <any>name);
        }
        if (id !== undefined && id !== null) {
            queryParameters = queryParameters.set('Id', <any>id);
        }

        let headers = this.defaultHeaders;

        // authentication (apikeyauth) required
        if (this.configuration.apiKeys["api_key"]) {
            queryParameters = queryParameters.set('api_key', this.configuration.apiKeys["api_key"]);
        }

        // authentication (embyauth) required
        if (this.configuration.accessToken) {
            const accessToken = typeof this.configuration.accessToken === 'function'
                ? this.configuration.accessToken()
                : this.configuration.accessToken;
            headers = headers.set('Authorization', 'Bearer ' + accessToken);
        }
        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
            'application/octet-stream'
        ];
        const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(consumes);
        if (httpContentTypeSelected != undefined) {
            headers = headers.set('Content-Type', httpContentTypeSelected);
        }

        return this.httpClient.request<any>('post',`${this.basePath}/Devices/CameraUploads`,
            {
                body: body,
                params: queryParameters,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * Updates device options
     * Requires authentication as administrator
     * @param body DeviceOptions: 
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public postDevicesOptions(body: DevicesDeviceOptions, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public postDevicesOptions(body: DevicesDeviceOptions, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public postDevicesOptions(body: DevicesDeviceOptions, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public postDevicesOptions(body: DevicesDeviceOptions, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (body === null || body === undefined) {
            throw new Error('Required parameter body was null or undefined when calling postDevicesOptions.');
        }

        let headers = this.defaultHeaders;

        // authentication (apikeyauth) required
        if (this.configuration.apiKeys["api_key"]) {
            queryParameters = queryParameters.set('api_key', this.configuration.apiKeys["api_key"]);
        }

        // authentication (embyauth) required
        if (this.configuration.accessToken) {
            const accessToken = typeof this.configuration.accessToken === 'function'
                ? this.configuration.accessToken()
                : this.configuration.accessToken;
            headers = headers.set('Authorization', 'Bearer ' + accessToken);
        }
        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
            'application/json',
            'application/xml'
        ];
        const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(consumes);
        if (httpContentTypeSelected != undefined) {
            headers = headers.set('Content-Type', httpContentTypeSelected);
        }

        return this.httpClient.request<any>('post',`${this.basePath}/Devices/Options`,
            {
                body: body,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

}
