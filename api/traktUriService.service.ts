/**
 * Emby Server API
 * Explore the Emby Server API
 *
 * OpenAPI spec version: 4.4.0.11
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *//* tslint:disable:no-unused-variable member-ordering */

import { Inject, Injectable, Optional }                      from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams,
         HttpResponse, HttpEvent }                           from '@angular/common/http';
import { CustomHttpUrlEncodingCodec }                        from '../encoder';

import { Observable }                                        from 'rxjs';


import { BASE_PATH, COLLECTION_FORMATS }                     from '../variables';
import { Configuration }                                     from '../configuration';


@Injectable()
export class TraktUriServiceService {

    protected basePath = 'https://stream.lebit.tech:8920/emby';
    public defaultHeaders = new HttpHeaders();
    public configuration = new Configuration();

    constructor(protected httpClient: HttpClient, @Optional()@Inject(BASE_PATH) basePath: string, @Optional() configuration: Configuration) {
        if (basePath) {
            this.basePath = basePath;
        }
        if (configuration) {
            this.configuration = configuration;
            this.basePath = basePath || configuration.basePath || this.basePath;
        }
    }

    /**
     * @param consumes string[] mime-types
     * @return true: consumes contains 'multipart/form-data', false: otherwise
     */
    private canConsumeForm(consumes: string[]): boolean {
        const form = 'multipart/form-data';
        for (const consume of consumes) {
            if (form === consume) {
                return true;
            }
        }
        return false;
    }


    /**
     * 
     * No authentication required
     * @param userId User Id
     * @param id Item Id
     * @param comment Text for the comment
     * @param spoiler Set to true to indicate the comment contains spoilers. Defaults to false
     * @param review Set to true to indicate the comment is a 200+ word review. Defaults to false
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public postTraktUsersByUseridItemsByIdComment(userId: string, id: string, comment: string, spoiler?: boolean, review?: boolean, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public postTraktUsersByUseridItemsByIdComment(userId: string, id: string, comment: string, spoiler?: boolean, review?: boolean, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public postTraktUsersByUseridItemsByIdComment(userId: string, id: string, comment: string, spoiler?: boolean, review?: boolean, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public postTraktUsersByUseridItemsByIdComment(userId: string, id: string, comment: string, spoiler?: boolean, review?: boolean, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (userId === null || userId === undefined) {
            throw new Error('Required parameter userId was null or undefined when calling postTraktUsersByUseridItemsByIdComment.');
        }

        if (id === null || id === undefined) {
            throw new Error('Required parameter id was null or undefined when calling postTraktUsersByUseridItemsByIdComment.');
        }

        if (comment === null || comment === undefined) {
            throw new Error('Required parameter comment was null or undefined when calling postTraktUsersByUseridItemsByIdComment.');
        }



        let queryParameters = new HttpParams({encoder: new CustomHttpUrlEncodingCodec()});
        if (comment !== undefined && comment !== null) {
            queryParameters = queryParameters.set('Comment', <any>comment);
        }
        if (spoiler !== undefined && spoiler !== null) {
            queryParameters = queryParameters.set('Spoiler', <any>spoiler);
        }
        if (review !== undefined && review !== null) {
            queryParameters = queryParameters.set('Review', <any>review);
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.request<any>('post',`${this.basePath}/Trakt/Users/${encodeURIComponent(String(userId))}/Items/${encodeURIComponent(String(id))}/Comment`,
            {
                params: queryParameters,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * 
     * No authentication required
     * @param userId User Id
     * @param id Item Id
     * @param rating Rating between 1 - 10 (0 &#x3D; unrate)
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public postTraktUsersByUseridItemsByIdRate(userId: string, id: string, rating: number, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public postTraktUsersByUseridItemsByIdRate(userId: string, id: string, rating: number, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public postTraktUsersByUseridItemsByIdRate(userId: string, id: string, rating: number, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public postTraktUsersByUseridItemsByIdRate(userId: string, id: string, rating: number, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (userId === null || userId === undefined) {
            throw new Error('Required parameter userId was null or undefined when calling postTraktUsersByUseridItemsByIdRate.');
        }

        if (id === null || id === undefined) {
            throw new Error('Required parameter id was null or undefined when calling postTraktUsersByUseridItemsByIdRate.');
        }

        if (rating === null || rating === undefined) {
            throw new Error('Required parameter rating was null or undefined when calling postTraktUsersByUseridItemsByIdRate.');
        }

        let queryParameters = new HttpParams({encoder: new CustomHttpUrlEncodingCodec()});
        if (rating !== undefined && rating !== null) {
            queryParameters = queryParameters.set('Rating', <any>rating);
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.request<any>('post',`${this.basePath}/Trakt/Users/${encodeURIComponent(String(userId))}/Items/${encodeURIComponent(String(id))}/Rate`,
            {
                params: queryParameters,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * 
     * No authentication required
     * @param userId User Id
     * @param genre Genre slug to filter by. (See http://trakt.tv/api-docs/genres-movies)
     * @param startYear 4-digit year to filter movies released this year or later
     * @param endYear 4-digit year to filter movies released this year or earlier
     * @param hideCollected Set true to hide movies in the users collection
     * @param hideWatchlisted Set true to hide movies in the users watchlist
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public postTraktUsersByUseridRecommendedmovies(userId: string, genre?: number, startYear?: number, endYear?: number, hideCollected?: boolean, hideWatchlisted?: boolean, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public postTraktUsersByUseridRecommendedmovies(userId: string, genre?: number, startYear?: number, endYear?: number, hideCollected?: boolean, hideWatchlisted?: boolean, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public postTraktUsersByUseridRecommendedmovies(userId: string, genre?: number, startYear?: number, endYear?: number, hideCollected?: boolean, hideWatchlisted?: boolean, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public postTraktUsersByUseridRecommendedmovies(userId: string, genre?: number, startYear?: number, endYear?: number, hideCollected?: boolean, hideWatchlisted?: boolean, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (userId === null || userId === undefined) {
            throw new Error('Required parameter userId was null or undefined when calling postTraktUsersByUseridRecommendedmovies.');
        }






        let queryParameters = new HttpParams({encoder: new CustomHttpUrlEncodingCodec()});
        if (genre !== undefined && genre !== null) {
            queryParameters = queryParameters.set('Genre', <any>genre);
        }
        if (startYear !== undefined && startYear !== null) {
            queryParameters = queryParameters.set('StartYear', <any>startYear);
        }
        if (endYear !== undefined && endYear !== null) {
            queryParameters = queryParameters.set('EndYear', <any>endYear);
        }
        if (hideCollected !== undefined && hideCollected !== null) {
            queryParameters = queryParameters.set('HideCollected', <any>hideCollected);
        }
        if (hideWatchlisted !== undefined && hideWatchlisted !== null) {
            queryParameters = queryParameters.set('HideWatchlisted', <any>hideWatchlisted);
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.request<any>('post',`${this.basePath}/Trakt/Users/${encodeURIComponent(String(userId))}/RecommendedMovies`,
            {
                params: queryParameters,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * 
     * No authentication required
     * @param userId User Id
     * @param genre Genre slug to filter by. (See http://trakt.tv/api-docs/genres-shows)
     * @param startYear 4-digit year to filter shows released this year or later
     * @param endYear 4-digit year to filter shows released this year or earlier
     * @param hideCollected Set true to hide shows in the users collection
     * @param hideWatchlisted Set true to hide shows in the users watchlist
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public postTraktUsersByUseridRecommendedshows(userId: string, genre?: number, startYear?: number, endYear?: number, hideCollected?: boolean, hideWatchlisted?: boolean, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public postTraktUsersByUseridRecommendedshows(userId: string, genre?: number, startYear?: number, endYear?: number, hideCollected?: boolean, hideWatchlisted?: boolean, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public postTraktUsersByUseridRecommendedshows(userId: string, genre?: number, startYear?: number, endYear?: number, hideCollected?: boolean, hideWatchlisted?: boolean, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public postTraktUsersByUseridRecommendedshows(userId: string, genre?: number, startYear?: number, endYear?: number, hideCollected?: boolean, hideWatchlisted?: boolean, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (userId === null || userId === undefined) {
            throw new Error('Required parameter userId was null or undefined when calling postTraktUsersByUseridRecommendedshows.');
        }






        let queryParameters = new HttpParams({encoder: new CustomHttpUrlEncodingCodec()});
        if (genre !== undefined && genre !== null) {
            queryParameters = queryParameters.set('Genre', <any>genre);
        }
        if (startYear !== undefined && startYear !== null) {
            queryParameters = queryParameters.set('StartYear', <any>startYear);
        }
        if (endYear !== undefined && endYear !== null) {
            queryParameters = queryParameters.set('EndYear', <any>endYear);
        }
        if (hideCollected !== undefined && hideCollected !== null) {
            queryParameters = queryParameters.set('HideCollected', <any>hideCollected);
        }
        if (hideWatchlisted !== undefined && hideWatchlisted !== null) {
            queryParameters = queryParameters.set('HideWatchlisted', <any>hideWatchlisted);
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.request<any>('post',`${this.basePath}/Trakt/Users/${encodeURIComponent(String(userId))}/RecommendedShows`,
            {
                params: queryParameters,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

}
