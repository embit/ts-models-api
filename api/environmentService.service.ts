/**
 * Emby Server API
 * Explore the Emby Server API
 *
 * OpenAPI spec version: 4.4.0.11
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *//* tslint:disable:no-unused-variable member-ordering */

import { Inject, Injectable, Optional }                      from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams,
         HttpResponse, HttpEvent }                           from '@angular/common/http';
import { CustomHttpUrlEncodingCodec }                        from '../encoder';

import { Observable }                                        from 'rxjs';

import { DefaultDirectoryBrowserInfo } from '../model/defaultDirectoryBrowserInfo';
import { IOFileSystemEntryInfo } from '../model/iOFileSystemEntryInfo';
import { ValidatePath } from '../model/validatePath';

import { BASE_PATH, COLLECTION_FORMATS }                     from '../variables';
import { Configuration }                                     from '../configuration';


@Injectable()
export class EnvironmentServiceService {

    protected basePath = 'https://stream.lebit.tech:8920/emby';
    public defaultHeaders = new HttpHeaders();
    public configuration = new Configuration();

    constructor(protected httpClient: HttpClient, @Optional()@Inject(BASE_PATH) basePath: string, @Optional() configuration: Configuration) {
        if (basePath) {
            this.basePath = basePath;
        }
        if (configuration) {
            this.configuration = configuration;
            this.basePath = basePath || configuration.basePath || this.basePath;
        }
    }

    /**
     * @param consumes string[] mime-types
     * @return true: consumes contains 'multipart/form-data', false: otherwise
     */
    private canConsumeForm(consumes: string[]): boolean {
        const form = 'multipart/form-data';
        for (const consume of consumes) {
            if (form === consume) {
                return true;
            }
        }
        return false;
    }


    /**
     * Gets the parent path of a given path
     * Requires authentication as administrator
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public getEnvironmentDefaultdirectorybrowser(observe?: 'body', reportProgress?: boolean): Observable<DefaultDirectoryBrowserInfo>;
    public getEnvironmentDefaultdirectorybrowser(observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<DefaultDirectoryBrowserInfo>>;
    public getEnvironmentDefaultdirectorybrowser(observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<DefaultDirectoryBrowserInfo>>;
    public getEnvironmentDefaultdirectorybrowser(observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        let headers = this.defaultHeaders;

        // authentication (apikeyauth) required
        if (this.configuration.apiKeys["api_key"]) {
            queryParameters = queryParameters.set('api_key', this.configuration.apiKeys["api_key"]);
        }

        // authentication (embyauth) required
        if (this.configuration.accessToken) {
            const accessToken = typeof this.configuration.accessToken === 'function'
                ? this.configuration.accessToken()
                : this.configuration.accessToken;
            headers = headers.set('Authorization', 'Bearer ' + accessToken);
        }
        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'application/json',
            'application/xml'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.request<DefaultDirectoryBrowserInfo>('get',`${this.basePath}/Environment/DefaultDirectoryBrowser`,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * Gets the contents of a given directory in the file system
     * Requires authentication as administrator
     * @param path 
     * @param includeFiles An optional filter to include or exclude files from the results. true/false
     * @param includeDirectories An optional filter to include or exclude folders from the results. true/false
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public getEnvironmentDirectorycontents(path: string, includeFiles?: boolean, includeDirectories?: boolean, observe?: 'body', reportProgress?: boolean): Observable<Array<IOFileSystemEntryInfo>>;
    public getEnvironmentDirectorycontents(path: string, includeFiles?: boolean, includeDirectories?: boolean, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<Array<IOFileSystemEntryInfo>>>;
    public getEnvironmentDirectorycontents(path: string, includeFiles?: boolean, includeDirectories?: boolean, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<Array<IOFileSystemEntryInfo>>>;
    public getEnvironmentDirectorycontents(path: string, includeFiles?: boolean, includeDirectories?: boolean, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (path === null || path === undefined) {
            throw new Error('Required parameter path was null or undefined when calling getEnvironmentDirectorycontents.');
        }



        let queryParameters = new HttpParams({encoder: new CustomHttpUrlEncodingCodec()});
        if (path !== undefined && path !== null) {
            queryParameters = queryParameters.set('Path', <any>path);
        }
        if (includeFiles !== undefined && includeFiles !== null) {
            queryParameters = queryParameters.set('IncludeFiles', <any>includeFiles);
        }
        if (includeDirectories !== undefined && includeDirectories !== null) {
            queryParameters = queryParameters.set('IncludeDirectories', <any>includeDirectories);
        }

        let headers = this.defaultHeaders;

        // authentication (apikeyauth) required
        if (this.configuration.apiKeys["api_key"]) {
            queryParameters = queryParameters.set('api_key', this.configuration.apiKeys["api_key"]);
        }

        // authentication (embyauth) required
        if (this.configuration.accessToken) {
            const accessToken = typeof this.configuration.accessToken === 'function'
                ? this.configuration.accessToken()
                : this.configuration.accessToken;
            headers = headers.set('Authorization', 'Bearer ' + accessToken);
        }
        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'application/json',
            'application/xml'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.request<Array<IOFileSystemEntryInfo>>('get',`${this.basePath}/Environment/DirectoryContents`,
            {
                params: queryParameters,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * Gets available drives from the server&#x27;s file system
     * Requires authentication as administrator
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public getEnvironmentDrives(observe?: 'body', reportProgress?: boolean): Observable<Array<IOFileSystemEntryInfo>>;
    public getEnvironmentDrives(observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<Array<IOFileSystemEntryInfo>>>;
    public getEnvironmentDrives(observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<Array<IOFileSystemEntryInfo>>>;
    public getEnvironmentDrives(observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        let headers = this.defaultHeaders;

        // authentication (apikeyauth) required
        if (this.configuration.apiKeys["api_key"]) {
            queryParameters = queryParameters.set('api_key', this.configuration.apiKeys["api_key"]);
        }

        // authentication (embyauth) required
        if (this.configuration.accessToken) {
            const accessToken = typeof this.configuration.accessToken === 'function'
                ? this.configuration.accessToken()
                : this.configuration.accessToken;
            headers = headers.set('Authorization', 'Bearer ' + accessToken);
        }
        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'application/json',
            'application/xml'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.request<Array<IOFileSystemEntryInfo>>('get',`${this.basePath}/Environment/Drives`,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * Gets a list of devices on the network
     * Requires authentication as administrator
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public getEnvironmentNetworkdevices(observe?: 'body', reportProgress?: boolean): Observable<Array<IOFileSystemEntryInfo>>;
    public getEnvironmentNetworkdevices(observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<Array<IOFileSystemEntryInfo>>>;
    public getEnvironmentNetworkdevices(observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<Array<IOFileSystemEntryInfo>>>;
    public getEnvironmentNetworkdevices(observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        let headers = this.defaultHeaders;

        // authentication (apikeyauth) required
        if (this.configuration.apiKeys["api_key"]) {
            queryParameters = queryParameters.set('api_key', this.configuration.apiKeys["api_key"]);
        }

        // authentication (embyauth) required
        if (this.configuration.accessToken) {
            const accessToken = typeof this.configuration.accessToken === 'function'
                ? this.configuration.accessToken()
                : this.configuration.accessToken;
            headers = headers.set('Authorization', 'Bearer ' + accessToken);
        }
        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'application/json',
            'application/xml'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.request<Array<IOFileSystemEntryInfo>>('get',`${this.basePath}/Environment/NetworkDevices`,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * Gets shares from a network device
     * Requires authentication as administrator
     * @param path 
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public getEnvironmentNetworkshares(path: string, observe?: 'body', reportProgress?: boolean): Observable<Array<IOFileSystemEntryInfo>>;
    public getEnvironmentNetworkshares(path: string, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<Array<IOFileSystemEntryInfo>>>;
    public getEnvironmentNetworkshares(path: string, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<Array<IOFileSystemEntryInfo>>>;
    public getEnvironmentNetworkshares(path: string, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (path === null || path === undefined) {
            throw new Error('Required parameter path was null or undefined when calling getEnvironmentNetworkshares.');
        }

        let queryParameters = new HttpParams({encoder: new CustomHttpUrlEncodingCodec()});
        if (path !== undefined && path !== null) {
            queryParameters = queryParameters.set('Path', <any>path);
        }

        let headers = this.defaultHeaders;

        // authentication (apikeyauth) required
        if (this.configuration.apiKeys["api_key"]) {
            queryParameters = queryParameters.set('api_key', this.configuration.apiKeys["api_key"]);
        }

        // authentication (embyauth) required
        if (this.configuration.accessToken) {
            const accessToken = typeof this.configuration.accessToken === 'function'
                ? this.configuration.accessToken()
                : this.configuration.accessToken;
            headers = headers.set('Authorization', 'Bearer ' + accessToken);
        }
        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'application/json',
            'application/xml'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.request<Array<IOFileSystemEntryInfo>>('get',`${this.basePath}/Environment/NetworkShares`,
            {
                params: queryParameters,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * Gets the parent path of a given path
     * Requires authentication as administrator
     * @param path 
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public getEnvironmentParentpath(path: string, observe?: 'body', reportProgress?: boolean): Observable<string>;
    public getEnvironmentParentpath(path: string, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<string>>;
    public getEnvironmentParentpath(path: string, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<string>>;
    public getEnvironmentParentpath(path: string, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (path === null || path === undefined) {
            throw new Error('Required parameter path was null or undefined when calling getEnvironmentParentpath.');
        }

        let queryParameters = new HttpParams({encoder: new CustomHttpUrlEncodingCodec()});
        if (path !== undefined && path !== null) {
            queryParameters = queryParameters.set('Path', <any>path);
        }

        let headers = this.defaultHeaders;

        // authentication (apikeyauth) required
        if (this.configuration.apiKeys["api_key"]) {
            queryParameters = queryParameters.set('api_key', this.configuration.apiKeys["api_key"]);
        }

        // authentication (embyauth) required
        if (this.configuration.accessToken) {
            const accessToken = typeof this.configuration.accessToken === 'function'
                ? this.configuration.accessToken()
                : this.configuration.accessToken;
            headers = headers.set('Authorization', 'Bearer ' + accessToken);
        }
        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'application/json',
            'application/xml'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.request<string>('get',`${this.basePath}/Environment/ParentPath`,
            {
                params: queryParameters,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * Gets the contents of a given directory in the file system
     * Requires authentication as administrator
     * @param body ValidatePath
     * @param path 
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public postEnvironmentValidatepath(body: ValidatePath, path: string, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public postEnvironmentValidatepath(body: ValidatePath, path: string, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public postEnvironmentValidatepath(body: ValidatePath, path: string, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public postEnvironmentValidatepath(body: ValidatePath, path: string, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (body === null || body === undefined) {
            throw new Error('Required parameter body was null or undefined when calling postEnvironmentValidatepath.');
        }

        if (path === null || path === undefined) {
            throw new Error('Required parameter path was null or undefined when calling postEnvironmentValidatepath.');
        }

        let queryParameters = new HttpParams({encoder: new CustomHttpUrlEncodingCodec()});
        if (path !== undefined && path !== null) {
            queryParameters = queryParameters.set('Path', <any>path);
        }

        let headers = this.defaultHeaders;

        // authentication (apikeyauth) required
        if (this.configuration.apiKeys["api_key"]) {
            queryParameters = queryParameters.set('api_key', this.configuration.apiKeys["api_key"]);
        }

        // authentication (embyauth) required
        if (this.configuration.accessToken) {
            const accessToken = typeof this.configuration.accessToken === 'function'
                ? this.configuration.accessToken()
                : this.configuration.accessToken;
            headers = headers.set('Authorization', 'Bearer ' + accessToken);
        }
        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
            'application/json',
            'application/xml'
        ];
        const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(consumes);
        if (httpContentTypeSelected != undefined) {
            headers = headers.set('Content-Type', httpContentTypeSelected);
        }

        return this.httpClient.request<any>('post',`${this.basePath}/Environment/ValidatePath`,
            {
                body: body,
                params: queryParameters,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

}
