/**
 * Emby Server API
 * Explore the Emby Server API
 *
 * OpenAPI spec version: 4.4.0.11
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *//* tslint:disable:no-unused-variable member-ordering */

import { Inject, Injectable, Optional }                      from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams,
         HttpResponse, HttpEvent }                           from '@angular/common/http';
import { CustomHttpUrlEncodingCodec }                        from '../encoder';

import { Observable }                                        from 'rxjs';

import { RokuMetadataApiThumbnailSetInfo } from '../model/rokuMetadataApiThumbnailSetInfo';

import { BASE_PATH, COLLECTION_FORMATS }                     from '../variables';
import { Configuration }                                     from '../configuration';


@Injectable()
export class BifServiceService {

    protected basePath = 'https://stream.lebit.tech:8920/emby';
    public defaultHeaders = new HttpHeaders();
    public configuration = new Configuration();

    constructor(protected httpClient: HttpClient, @Optional()@Inject(BASE_PATH) basePath: string, @Optional() configuration: Configuration) {
        if (basePath) {
            this.basePath = basePath;
        }
        if (configuration) {
            this.configuration = configuration;
            this.basePath = basePath || configuration.basePath || this.basePath;
        }
    }

    /**
     * @param consumes string[] mime-types
     * @return true: consumes contains 'multipart/form-data', false: otherwise
     */
    private canConsumeForm(consumes: string[]): boolean {
        const form = 'multipart/form-data';
        for (const consume of consumes) {
            if (form === consume) {
                return true;
            }
        }
        return false;
    }


    /**
     * 
     * Requires authentication as user
     * @param width 
     * @param id Item Id
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public getItemsByIdThumbnailset(width: number, id: string, observe?: 'body', reportProgress?: boolean): Observable<RokuMetadataApiThumbnailSetInfo>;
    public getItemsByIdThumbnailset(width: number, id: string, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<RokuMetadataApiThumbnailSetInfo>>;
    public getItemsByIdThumbnailset(width: number, id: string, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<RokuMetadataApiThumbnailSetInfo>>;
    public getItemsByIdThumbnailset(width: number, id: string, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (width === null || width === undefined) {
            throw new Error('Required parameter width was null or undefined when calling getItemsByIdThumbnailset.');
        }

        if (id === null || id === undefined) {
            throw new Error('Required parameter id was null or undefined when calling getItemsByIdThumbnailset.');
        }

        let queryParameters = new HttpParams({encoder: new CustomHttpUrlEncodingCodec()});
        if (width !== undefined && width !== null) {
            queryParameters = queryParameters.set('Width', <any>width);
        }

        let headers = this.defaultHeaders;

        // authentication (apikeyauth) required
        if (this.configuration.apiKeys["api_key"]) {
            queryParameters = queryParameters.set('api_key', this.configuration.apiKeys["api_key"]);
        }

        // authentication (embyauth) required
        if (this.configuration.accessToken) {
            const accessToken = typeof this.configuration.accessToken === 'function'
                ? this.configuration.accessToken()
                : this.configuration.accessToken;
            headers = headers.set('Authorization', 'Bearer ' + accessToken);
        }
        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'application/json',
            'application/xml'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.request<RokuMetadataApiThumbnailSetInfo>('get',`${this.basePath}/Items/${encodeURIComponent(String(id))}/ThumbnailSet`,
            {
                params: queryParameters,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * 
     * No authentication required
     * @param width 
     * @param id Item Id
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public getVideosByIdIndexBif(width: number, id: string, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public getVideosByIdIndexBif(width: number, id: string, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public getVideosByIdIndexBif(width: number, id: string, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public getVideosByIdIndexBif(width: number, id: string, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (width === null || width === undefined) {
            throw new Error('Required parameter width was null or undefined when calling getVideosByIdIndexBif.');
        }

        if (id === null || id === undefined) {
            throw new Error('Required parameter id was null or undefined when calling getVideosByIdIndexBif.');
        }

        let queryParameters = new HttpParams({encoder: new CustomHttpUrlEncodingCodec()});
        if (width !== undefined && width !== null) {
            queryParameters = queryParameters.set('Width', <any>width);
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.request<any>('get',`${this.basePath}/Videos/${encodeURIComponent(String(id))}/index.bif`,
            {
                params: queryParameters,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

}
