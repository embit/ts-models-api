import { NgModule, ModuleWithProviders, SkipSelf, Optional } from '@angular/core';
import { Configuration } from './configuration';
import { HttpClient } from '@angular/common/http';


import { ActivityLogServiceService } from './api/activityLogService.service';
import { ArtistsServiceService } from './api/artistsService.service';
import { AudioServiceService } from './api/audioService.service';
import { BifServiceService } from './api/bifService.service';
import { BoxSetsHandlerService } from './api/boxSetsHandler.service';
import { BrandingServiceService } from './api/brandingService.service';
import { ChannelServiceService } from './api/channelService.service';
import { CollectionServiceService } from './api/collectionService.service';
import { ConfigurationServiceService } from './api/configurationService.service';
import { ConnectServiceService } from './api/connectService.service';
import { DashboardServiceService } from './api/dashboardService.service';
import { DeviceServiceService } from './api/deviceService.service';
import { DiskSpaceServiceService } from './api/diskSpaceService.service';
import { DisplayPreferencesServiceService } from './api/displayPreferencesService.service';
import { DlnaServerServiceService } from './api/dlnaServerService.service';
import { DlnaServiceService } from './api/dlnaService.service';
import { DynamicHlsServiceService } from './api/dynamicHlsService.service';
import { EncodingInfoServiceService } from './api/encodingInfoService.service';
import { EnvironmentServiceService } from './api/environmentService.service';
import { GameGenresServiceService } from './api/gameGenresService.service';
import { GamesServiceService } from './api/gamesService.service';
import { GenresServiceService } from './api/genresService.service';
import { HlsSegmentServiceService } from './api/hlsSegmentService.service';
import { IgnoreHandlerService } from './api/ignoreHandler.service';
import { ImageByNameServiceService } from './api/imageByNameService.service';
import { ImageHandlerService } from './api/imageHandler.service';
import { ImageServiceService } from './api/imageService.service';
import { InstantMixServiceService } from './api/instantMixService.service';
import { ItemLookupServiceService } from './api/itemLookupService.service';
import { ItemRefreshServiceService } from './api/itemRefreshService.service';
import { ItemUpdateServiceService } from './api/itemUpdateService.service';
import { ItemsServiceService } from './api/itemsService.service';
import { LibraryServiceService } from './api/libraryService.service';
import { LibraryStructureServiceService } from './api/libraryStructureService.service';
import { LiveTvServiceService } from './api/liveTvService.service';
import { LocalizationServiceService } from './api/localizationService.service';
import { MapHandlerService } from './api/mapHandler.service';
import { MediaInfoServiceService } from './api/mediaInfoService.service';
import { MoviesServiceService } from './api/moviesService.service';
import { MusicGenresServiceService } from './api/musicGenresService.service';
import { NewsServiceService } from './api/newsService.service';
import { NotificationsServiceService } from './api/notificationsService.service';
import { OfficialRatingServiceService } from './api/officialRatingService.service';
import { OpenApiServiceService } from './api/openApiService.service';
import { PackageServiceService } from './api/packageService.service';
import { PersonsServiceService } from './api/personsService.service';
import { PlaylistServiceService } from './api/playlistService.service';
import { PlaystateServiceService } from './api/playstateService.service';
import { PluginServiceService } from './api/pluginService.service';
import { ProfileHandlerService } from './api/profileHandler.service';
import { RemoteImageServiceService } from './api/remoteImageService.service';
import { ReportsServiceService } from './api/reportsService.service';
import { ScheduledTaskServiceService } from './api/scheduledTaskService.service';
import { SearchServiceService } from './api/searchService.service';
import { ServerApiEndpointsService } from './api/serverApiEndpoints.service';
import { SessionsServiceService } from './api/sessionsService.service';
import { StudiosServiceService } from './api/studiosService.service';
import { SubtitleServiceService } from './api/subtitleService.service';
import { SuggestionsServiceService } from './api/suggestionsService.service';
import { SyncServiceService } from './api/syncService.service';
import { SystemServiceService } from './api/systemService.service';
import { TagServiceService } from './api/tagService.service';
import { TrailersServiceService } from './api/trailersService.service';
import { TraktUriServiceService } from './api/traktUriService.service';
import { TvShowsServiceService } from './api/tvShowsService.service';
import { UniversalAudioServiceService } from './api/universalAudioService.service';
import { UserActivityAPIService } from './api/userActivityAPI.service';
import { UserLibraryServiceService } from './api/userLibraryService.service';
import { UserServiceService } from './api/userService.service';
import { UserViewsServiceService } from './api/userViewsService.service';
import { VideoHlsServiceService } from './api/videoHlsService.service';
import { VideoServiceService } from './api/videoService.service';
import { VideosServiceService } from './api/videosService.service';

@NgModule({
  imports:      [],
  declarations: [],
  exports:      [],
  providers: [
    ActivityLogServiceService,
    ArtistsServiceService,
    AudioServiceService,
    BifServiceService,
    BoxSetsHandlerService,
    BrandingServiceService,
    ChannelServiceService,
    CollectionServiceService,
    ConfigurationServiceService,
    ConnectServiceService,
    DashboardServiceService,
    DeviceServiceService,
    DiskSpaceServiceService,
    DisplayPreferencesServiceService,
    DlnaServerServiceService,
    DlnaServiceService,
    DynamicHlsServiceService,
    EncodingInfoServiceService,
    EnvironmentServiceService,
    GameGenresServiceService,
    GamesServiceService,
    GenresServiceService,
    HlsSegmentServiceService,
    IgnoreHandlerService,
    ImageByNameServiceService,
    ImageHandlerService,
    ImageServiceService,
    InstantMixServiceService,
    ItemLookupServiceService,
    ItemRefreshServiceService,
    ItemUpdateServiceService,
    ItemsServiceService,
    LibraryServiceService,
    LibraryStructureServiceService,
    LiveTvServiceService,
    LocalizationServiceService,
    MapHandlerService,
    MediaInfoServiceService,
    MoviesServiceService,
    MusicGenresServiceService,
    NewsServiceService,
    NotificationsServiceService,
    OfficialRatingServiceService,
    OpenApiServiceService,
    PackageServiceService,
    PersonsServiceService,
    PlaylistServiceService,
    PlaystateServiceService,
    PluginServiceService,
    ProfileHandlerService,
    RemoteImageServiceService,
    ReportsServiceService,
    ScheduledTaskServiceService,
    SearchServiceService,
    ServerApiEndpointsService,
    SessionsServiceService,
    StudiosServiceService,
    SubtitleServiceService,
    SuggestionsServiceService,
    SyncServiceService,
    SystemServiceService,
    TagServiceService,
    TrailersServiceService,
    TraktUriServiceService,
    TvShowsServiceService,
    UniversalAudioServiceService,
    UserActivityAPIService,
    UserLibraryServiceService,
    UserServiceService,
    UserViewsServiceService,
    VideoHlsServiceService,
    VideoServiceService,
    VideosServiceService ]
})
export class ApiModule {
    public static forRoot(configurationFactory: () => Configuration): ModuleWithProviders {
        return {
            ngModule: ApiModule,
            providers: [ { provide: Configuration, useFactory: configurationFactory } ]
        };
    }

    constructor( @Optional() @SkipSelf() parentModule: ApiModule,
                 @Optional() http: HttpClient) {
        if (parentModule) {
            throw new Error('ApiModule is already loaded. Import in your base AppModule only.');
        }
        if (!http) {
            throw new Error('You need to import the HttpClientModule in your AppModule! \n' +
            'See also https://github.com/angular/angular/issues/20575');
        }
    }
}
